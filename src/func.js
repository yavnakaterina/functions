const getSum = (str1, str2) => {
    const condition1 = typeof str1 !== "string" || typeof str2 !== "string";
    const condition2 = isNaN(parseInt(str1));
    const condition3 = isNaN(parseInt(str2));
    if (condition1) return false;
    if (str1.length === 0) str1 = Number(str1);
    else if (condition2) return false;
    else str1 = parseInt(str1);
    if (str2.length === 0) str2 = Number(str2);
    else if (condition3) return false;
    else str2 = parseInt(str2);
    return (str1 + str2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    let resultPosts = listOfPosts.reduce(function (acc, currValue) {
        const condition = currValue.author === authorName;
        if (condition) acc += 1;
        return acc;
    }, 0);

    const listComments = listOfPosts.reduce((a, c) => [...a, c.comments], [])
        .reduce((a, arr) => a.concat(arr)).filter(x => x !== undefined);

    let resultComments = listComments.reduce(function (acc, currvalue) {
        const condition = currvalue.author === authorName;
        if (condition) acc += 1;
        return acc;
    }, 0);
    return `Post:${resultPosts},comments:${resultComments}`;
};

const tickets = (people) => {

    let cash25 = 0;
    let cash50 = 0;

    for (let i = 0; i <= people.length - 1; i++) {
        if (people[i] == 25) {
            cash25 += 1;
        }
        if (people[i] == 50) {
            if (cash25 > 0) {
                cash50 += 1;
                cash25 -= 1;
            }
            else return "NO";
        }
        if (people[i] == 100) {
            if (cash25 > 1 && cash50 == 0) {
                cash25 -= 2;
            }
            else if (cash25 > 0 && cash50 > 0) {
                cash25 -= 1;
                cash50 -= 1;
            }
            else return "NO";
        }
    }
    return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
